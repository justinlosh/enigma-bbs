FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code
WORKDIR /app/code

# copy code
ADD package.json server.js start.sh /app/code/

# install packages
RUN npm install
#    && dos2unix /enigma-bbs/docker/bin/docker-entrypoint.sh \
#    && apt-get remove dos2unix -y \
#    && chmod +x /enigma-bbs/docker/bin/docker-entrypoint.sh \
#    && cp -f /enigma-bbs/docker/bin/sexyz /usr/local/bin \
#    && cd /app/code/enigma-bbs \
#    && pm2 start main.js \
#    && mkdir -p /app/code/enigma-bbs-pre/art \
#    && mkdir /app/code/enigma-bbs-pre/mods \
#    && mkdir /app/code/enigma-bbs-pre/config \
#    && cp -rp art/* ../enigma-bbs-pre/art/ \
#    && cp -rp mods/* ../enigma-bbs-pre/mods/ \
#    && cp -rp config/* ../enigma-bbs-pre/config/ \
#    && apt-get remove build-essential python3 libssl-dev git curl -y \
#    && apt-get autoremove -y \
#    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
#    && apt-get clean

# enigma storage mounts
VOLUME /app/code/enigma-bbs/art
VOLUME /app/code/enigma-bbs/config
VOLUME /app/code/enigma-bbs/db
VOLUME /app/code/enigma-bbs/filebase
VOLUME /app/code/enigma-bbs/logs
VOLUME /app/code/enigma-bbs/mods
VOLUME /app/code/mail

CMD [ "/app/code/start.sh" ]

# Enigma default port
# EXPOSE 8888